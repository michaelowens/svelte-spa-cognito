import { Auth } from 'aws-amplify'
import { user, userState } from '../store'
import { to } from '../utils'

export async function checkAuthenticatedUser() {
  console.log('check user')
  let getUser = Auth.currentAuthenticatedUser({ bypassCache: false })
  let [awsUser, err] = await to(getUser)
  if (err) return
  console.log(awsUser)
  user.set(awsUser)
  userState.set('signedIn')
}

export async function signIn(username, password) {
  const awsUser = await Auth.signIn(username, password)
  console.log(awsUser)
  if (
    user.challengeName === 'SMS_MFA' ||
    user.challengeName === 'SOFTWARE_TOKEN_MFA'
  ) {
    userState.set('confirmSignIn')
    // const loggedUser = await Auth.confirmSignIn(
    //   awsUser, // Return object from Auth.signIn()
    //   code, // Confirmation code
    //   mfaType // MFA Type e.g. SMS_MFA, SOFTWARE_TOKEN_MFA
    // );
    console.log('MFA required')
  } else if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
    userState.set('requireNewPassword')
    console.log('new password required')
  } else if (user.challengeName === 'MFA_SETUP') {
    userState.set('mfaSetup')
    console.log('MFA TOTP setup required')
  } else {
    user.set(awsUser)
    userState.set('signedIn')
    return [user, null]
  }
}

export async function signUp(options) {
  try {
    const response = await Auth.signUp(options)
    console.log(response)
  } catch (err) {
    console.log(err)
  }
}

export function signOut() {
  user.set({})
  userState.set('signIn')
  return Auth.signOut()
}
