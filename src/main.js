import Amplify from 'aws-amplify'
import awsconfig from './aws-exports'
import { checkAuthenticatedUser } from './components/auth'
import { userState, url } from './store'
import { get } from 'svelte/store'
import { navigate } from 'svelte-routing'
import App from './App.svelte'
import { needsAuth } from './routes'

Amplify.configure(awsconfig)

const app = new App({
  target: document.body,
  hydrate: true,
})

checkAuthenticatedUser().then(() => {
  const unsub = userState.subscribe(value => {
    let currentUrl = location.pathname
    let path = currentUrl

    if (needsAuth(path) && value != 'signedIn') path = '/signin'
    if (path !== currentUrl && currentUrl !== '/')
      path += '?redirect=' + encodeURIComponent(currentUrl)
    if (value == 'signedIn')
      path = currentUrl == '/' ? '/dashboard' : currentUrl

    if (path !== currentUrl) {
      navigate(path, { replace: true })
    }
  })
})

export default app
