import { Error404, Dashboard, About, SignIn, SignUp } from './pages'

const routes = [
  {
    path: 'about',
    component: About,
  },
  {
    path: 'dashboard',
    component: Dashboard,
  },
  {
    path: 'signin',
    component: SignIn,
    props: { noAuth: true },
  },
  {
    path: 'signup',
    component: SignUp,
    props: { noAuth: true },
  },
  {
    path: '',
    component: Error404,
  },
]

export default routes

export function needsAuth(path) {
  if (path[0] === '/') path = path.substr(1)
  const route = routes.find(route => route.path === path)
  return !route.props || !route.props.noAuth
}
