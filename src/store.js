import { writable } from 'svelte/store'

export const url = writable('')
export const user = writable({})
export const userState = writable('signIn')
