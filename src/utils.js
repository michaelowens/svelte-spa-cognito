export function to(promise) {
  return promise.then((...args) => [...args, null]).catch(err => [null, err])
}
