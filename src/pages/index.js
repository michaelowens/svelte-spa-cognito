import Error404 from './Error404.svelte'
import Dashboard from './Dashboard.svelte'
import About from './About.svelte'
import SignIn from './SignIn.svelte'
import SignUp from './SignUp.svelte'

export { Error404, Dashboard, About, SignIn, SignUp }
